import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Chinchilla from './components/component-1';
import Chinchille from './components/component-2';
import ShowApi from './components/component-3';
import HookedTextField from './components/component-3';

export default function App() {
  return (
    <View style={styles.container}>
      {/* <Chinchilla name="Martin"/> */}
      <ShowApi />
    </View>
  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
