import { Button, FormControl, List, ListItem, ListItemProps, ListItemText, TextField } from '@material-ui/core';
import React, { constructor, useState } from 'react';
import { useForm } from 'react-hook-form';
import { FlatList, StyleSheet, Text } from 'react-native';

interface Inputs {
    name: string;
}

function ListItemLink(props: ListItemProps<'a', { button?: true }>) {
    return <ListItem button component="a" {...props} />;
}

// let namesArray = Array<string>();
// let namesArray = ['hola', 'sad'];


const Chinchilla = (props: any) => {

    const { register, handleSubmit } = useForm<Inputs>();

    const [namesArray, setNamesArray] = useState<Array<string>>([]);


    const addName = async (data: Inputs) => {
        console.log(data);
        setNamesArray([...namesArray, data.name]);
    };

    // const styles = StyleSheet.create({
    //     container: {
    //         flex: 1,
    //         paddingTop: 22
    //     },
    //     item: {
    //         padding: 10,
    //         fontSize: 18,
    //         height: 44,
    //     },
    // });



    return (
        <>
            <Text>Hola, {props.name}</Text>
            <form onSubmit={handleSubmit(addName)}>
                <FormControl>
                    <TextField id="hame" label="Outlined" name="name" variant="outlined" inputRef={register} />
                    <Button variant="outlined" color="primary" type={'submit'}>Agregar</Button>
                </FormControl>
            </form>
            {/* <FlatList
                data={namesArray}
                renderItem={({ item }) => <Text style={styles.item}>{item}</Text>}
            /> */}
            <List component="nav" aria-label="secondary mailbox folders">
                {namesArray.map((item, index) => (
                    <ListItem key={`${index}_${item}`} button>
                        <ListItemText primary={item} />
                    </ListItem>
                ))}
            </List>
        </>
    );
}

export default Chinchilla;