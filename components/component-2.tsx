import { Button, FormControl, InputLabel, OutlinedInput, TextField } from '@material-ui/core';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';

const consumeApi = () => {

  return fetch('https://mindicador.cl/api/bitcoin')
    .then((response) => response.json())
    .then((json) => {
      return json.serie;
    })
    .catch((error) => {
      console.error(error);
    });
}

export default consumeApi;