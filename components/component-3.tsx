import React, { useState } from 'react';
import { Button, List, ListItem, ListItemProps, ListItemText, TextField } from '@material-ui/core';
import { useForm } from 'react-hook-form';
import consumeApi from './component-2';

function ListItemLink(props: ListItemProps<'a', { button?: true }>) {
  return <ListItem button component="a" {...props} />;
}



const ShowApi = async () => {

  function getSerie() {
    return fetch('https://mindicador.cl/api/bitcoin')
      .then((response) => response.json())
      .then((json) => {
        return json.serie;
      })
      .catch((error) => {
        console.error(error);
      });
  }


  const [prices, setPrices] = useState<Array<number>>([]);

  setPrices([...prices, ...(await getSerie()).map((item: { valor: any; }) => item.valor)]);

  const showData = () => {
    //   serie.then((series) => {
    //     const newPrices = series.map((item: { valor: any; }) => item.valor);
    //     console.log(newPrices);
    //     setPrices([...prices, ...newPrices]);
    //     console.log("serie.then -> prices", prices);
    //   }).catch((e) => {
    //     console.log("serie.then -> e", e);
    //   }).finally(() => {
    //     console.log("Funcion ejecutada");
    // });

  }

  return (
    <>
      <Button variant="outlined" color="primary" onClick={showData}>Mostrar</Button>

      <List component="nav" aria-label="secondary mailbox folders">
        {prices.map((item, index) => (
          <ListItem key={`${index}_${item}`} button>
            <ListItemText primary={`Precio: ${item}`} />
          </ListItem>
        ))}
      </List>
    </>
  );
};

export default ShowApi;